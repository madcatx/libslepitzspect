#ifndef LIBSLEPITZSPECT_H
#define LIBSLEPITZSPECT_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#ifdef _WIN32
	#define LSPTZ_CC __stdcall
	#define LSPTZ_API __declspec(dllimport)
#else
	#ifdef __i386__
		#define LSPTZ_CC __attribute((__cdecl__))
	#else
		#define LSPTZ_CC
	#endif /* __i386__ */
	#define LSPTZ_API
#endif /* _WIN32 */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


enum LSPTZ_ReadErrors {
	LSPTZ_CannotOpen,
	LSPTZ_CannotRead,
	LSPTZ_CannotSeek,
	LSPTZ_InvalidCall,
	LSPTZ_InvalidDate,
	LSPTZ_InvalidFileSize,
	LSPTZ_InvalidFrame,
	LSPTZ_InvalidString,
	LSPTZ_Unsupported,
	LSPTZ_WrongFileType
};

enum LSPTZ_Result {
	LSPTZ_Success,
	LSPTZ_Failure
};

struct LSPTZ_Metadata {
	const char *sample_name;
	const char *operator_name;
	const char *units;
	const char *signal_origin;
	const char *method_name;
	uint16_t run_number;
	uint8_t day;
	uint8_t month;
	uint16_t year;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
};

struct LSPTZ_SpectPoint {
	uint32_t wavelength;
	float absorbance;
};

struct LSPTZ_Spectrum {
	float time;
	const struct LSPTZ_SpectPoint *spectrum;
	uintptr_t n_points;
};

struct LSPTZ_Trace {
	struct LSPTZ_Metadata metadata;
	const struct LSPTZ_Spectrum *spectra;
	uintptr_t n_spectra;
};

LSPTZ_API const char * LSPTZ_CC lsptz_error_string(enum LSPTZ_ReadErrors error);
LSPTZ_API void LSPTZ_CC lsptz_free(struct LSPTZ_Trace *trace);
LSPTZ_API enum LSPTZ_Result LSPTZ_CC lsptz_read(const char *path,
						struct LSPTZ_Trace *trace,
						enum LSPTZ_ReadErrors *error);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* LIBSLEPITZSPECT_H */
