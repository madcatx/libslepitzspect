extern crate slepitzspect;

use std::env;
use std::process;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufWriter;

fn dump(trace: &slepitzspect::Trace, fh: &File) -> std::result::Result<(), std::io::Error> {
    let mut wrt = BufWriter::new(fh);

    wrt.write("time; ".as_bytes())?;
    for pt in &trace.spectra[0].spectrum {
        wrt.write(pt.wavelength.to_string().as_bytes())?;
        wrt.write("; ".as_bytes())?;
    }
    wrt.write("\n".as_bytes())?;

    for pt in &trace.spectra {
        wrt.write(pt.time.to_string().as_bytes())?;
        wrt.write("; ".as_bytes())?;

        for sp in &pt.spectrum {
            wrt.write(sp.absorbance.to_string().as_bytes())?;
            wrt.write("; ".as_bytes())?;
        }
        wrt.write("\n".as_bytes())?;
    }

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("No input path was given, exiting...");
        process::exit(1)
    }

    if args.len() < 3 {
        println!("No output path was given, exiting...");
        process::exit(1)
    }

    let fh = match File::create(&args[2]) {
        Err(_) => process::exit(1),
        Ok(r) => r
    };

    let trace = match slepitzspect::read(&args[1]) {
        Err(e) => panic!("ERROR: {}", slepitzspect::error_string(e)),
        Ok(r) => r
    };

    dump(&trace, &fh).expect("Cannot write output");
}
