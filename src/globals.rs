#[repr(C)]
pub enum ReadErrors {
    CannotOpen,
    CannotRead,
    CannotSeek,
    InvalidDate,
    InvalidFileSize,
    InvalidFrame,
    InvalidString,
    Unsupported,
    WrongFileType,
}

pub enum UVFileType {
    Type31,
    Type131,
}

pub struct Metadata {
    pub sample_name: String,
    pub operator_name: String,
    pub units: String,
    pub signal_origin: String,
    pub method_name: String,
    pub run_number: u16,
    pub day: u8,
    pub month: u8,
    pub year: u16,
    pub hour: u8,
    pub minute: u8,
    pub second: u8,
}

pub struct SpectPoint {
    pub wavelength: u32,
    pub absorbance: f32,
}

pub type SpectPointVec = Vec<SpectPoint>;

pub struct Spectrum {
    pub time: f32,
    pub spectrum: SpectPointVec,
}

pub struct Trace {
    pub metadata: Metadata,
    pub spectra: Vec<Spectrum>,
}
