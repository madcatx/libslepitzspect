use std::mem;
use globals::*;
use file_ops::*;

const OFFSET_RUN_NUMBER: u64 = 0x0100;
const _OFFSET_BLOCK_44_BEG: u64 = 0x0104;    /* Block 44 contains indices into block 43 */
const OFFSET_NUM_FRAMES: u64 = 0x0118;
const OFFSET_BLOCK_43_BEG: u64 = 0x0140;     /* Block 43 contains actual spectral data */
const OFFSET_SAMPLE_NAME: u64 = 0x035A;
const OFFSET_OPERATOR_NAME: u64 = 0x0758;
const OFFSET_DATE_STR: u64 = 0x0957;
const OFFSET_METHOD_NAME: u64 = 0x0A0E;
const OFFSET_SCALE_FACTOR: u64 = 0x0C0D;
const OFFSET_UNITS: u64 = 0x0C15;
const OFFSET_SIGNAL_ORIGIN: u64 = 0x0C40;

const BLOCK_43_SIG: u16 = 0x0043;
const BLOCK_44_SIG: u16 = 0x0044;
const SET_ABSORBANCE_ABSOLUTE: i16 = -32768;

const BLOCK_43_HEADER_LENGTH: usize = 22;

enum EndOfSpectra {
    Yes,
    No,
}

struct Interpretation {
    data_offset: u64,
    scaling: f32,
    num_frames: usize,
}

type DateResult = Result<(u8,   /* Day */
                          u8,   /* Month */
                          u16,  /* Year */
                          u8,   /* Hour */
                          u8,   /* Minute */
                          u8,   /* Second */
                          ), ReadErrors>;

type FrameResult = Result<(Spectrum, EndOfSpectra), ReadErrors>;

fn read_date<T: ReadSeek>(rdr: &mut T) -> DateResult {
    let mut s = read_utf16_string_at::<T>(rdr, OFFSET_DATE_STR)?;

    /* Extract day from date-time string */
    let mut idx = match s.find("-") {
        None => return Err(ReadErrors::InvalidDate),
        Some(idx) => idx,
    };
    let mut sub = s[0 .. idx].to_string();
    let day = match sub.parse::<u8>() {
        Err(_) => return Err(ReadErrors::InvalidDate),
        Ok(v) => v,
    };

    /* Extract month from date-time string */
    s = s[idx+1 ..].to_string();
    idx = match s.find("-") {
        None => return Err(ReadErrors::InvalidDate),
        Some(idx) => idx,
    };
    sub = s[0 .. idx].to_string();
    let month_str = sub.to_lowercase();
    let month = match month_str.as_ref() {
        "jan" => 0,
        "feb" => 1,
        "mar" => 2,
        "apr" => 3,
        "may" => 4,
        "jun" => 5,
        "jul" => 6,
        "aug" => 7,
        "sep" => 8,
        "oct" => 9,
        "nov" => 10,
        "dec" => 11,
        _ => return Err(ReadErrors::InvalidDate),
    };

    /* Extract year from date-time string */
    s = s[idx+1 ..].to_string();
    idx = match s.find(",") {
        None => return Err(ReadErrors::InvalidDate),
        Some(idx) => idx,
    };
    sub = s[0 .. idx].to_string();
    let year = match sub.parse::<u16>() {
        Err(_) => return Err(ReadErrors::InvalidDate),
        Ok(year) => if year < 80 {
            year + 2000
        } else {
            year + 1900
        },
    };

    /* Skip to the time part */
    s = s[idx+1 ..].to_string();
    idx = match s.find(" ") {
        None => return Err(ReadErrors::InvalidDate),
        Some(idx) => idx,
    };

    /* Extract hour from date-time string */
    s = s[idx+1 ..].to_string();
    idx = match s.find(":") {
        None => return Err(ReadErrors::InvalidDate),
        Some(idx) => idx,
    };
    sub = s[0 .. idx].to_string();
    let hour = match sub.parse::<u8>() {
        Err(_) => return Err(ReadErrors::InvalidDate),
        Ok(hr) => hr,
    };

    /* Extract minute from date-time string */
    s = s[idx+1 ..].to_string();
    idx = match s.find(":") {
        None => return Err(ReadErrors::InvalidDate),
        Some(idx) => idx,
    };
    sub = s[0 .. idx].to_string();
    let minute = match sub.parse::<u8>() {
        Err(_) => return Err(ReadErrors::InvalidDate),
        Ok(min) => min,
    };

    /* Extract second from date-time string */
    s = s[idx+1 ..].to_string();
    let second = match s.parse::<u8>() {
        Err(_) => return Err(ReadErrors::InvalidDate),
        Ok(sec) => sec,
    };

    Ok((day, month, year, hour, minute, second))
}

fn read_interpretation_data<T: ReadSeek>(rdr: &mut T) -> Result<Interpretation, ReadErrors> {
    /* Read where the data block begins */
    let data_offset = read_value_at::<u16, T>(rdr, OFFSET_BLOCK_43_BEG, Endianess::Big)?;
    let num_frames = read_value_at::<u16, T>(rdr, OFFSET_NUM_FRAMES, Endianess::Big)?;
    let scaling = read_value_at::<f64, T>(rdr, OFFSET_SCALE_FACTOR, Endianess::Big)? as f32;

    Ok(Interpretation { data_offset: data_offset.into(),
                        scaling: scaling,
                        num_frames: num_frames as usize })
}

fn read_metadata<T: ReadSeek>(rdr: &mut T) -> Result<Metadata, ReadErrors> {
    let run_no = read_value_at::<u16, T>(rdr, OFFSET_RUN_NUMBER, Endianess::Big)?; /* This is zero for single runs */
    let sam_name = read_utf16_string_at::<T>(rdr, OFFSET_SAMPLE_NAME)?;
    let op_name = read_utf16_string_at::<T>(rdr, OFFSET_OPERATOR_NAME)?;
    let units = read_utf16_string_at::<T>(rdr, OFFSET_UNITS)?;
    let date = read_date::<T>(rdr)?;
    let method = read_utf16_string_at::<T>(rdr, OFFSET_METHOD_NAME)?;
    let origin = read_utf16_string_at::<T>(rdr, OFFSET_SIGNAL_ORIGIN)?;

    Ok(Metadata {
        operator_name: op_name,
        sample_name: sam_name,
        units: units,
        signal_origin: origin,
        method_name: method,
        run_number: run_no,
        day: date.0,
        month: date.1,
        year: date.2,
        hour: date.3,
        minute: date.4,
        second: date.5
        })
}

fn read_frame<T: ReadSeek>(rdr: &mut T, scaling: f32) -> FrameResult {
    let mut spectrum: SpectPointVec = Vec::new();

    let frame_sig = read_value::<u16, T>(rdr, Endianess::Little)?;

    if frame_sig == BLOCK_44_SIG {
        return Ok((Spectrum { time: -1.0, spectrum: spectrum }, EndOfSpectra::Yes ));
    }

    if frame_sig != BLOCK_43_SIG {
        return Err(ReadErrors::InvalidFrame)
    }

    let frame_length = read_value::<u16, T>(rdr, Endianess::Little)? as usize;

    let timestamp = read_value::<u32, T>(rdr, Endianess::Little)?;
    let wl_low = (read_value::<u16, T>(rdr, Endianess::Little)? as u32) * 50;      /* Convert to picometers */
    let wl_high = (read_value::<u16, T>(rdr, Endianess::Little)? as u32) * 50;     /* Convert to picometers */
    let wl_step = (read_value::<u16, T>(rdr, Endianess::Little)? as u32) * 50;     /* Convert to picometers */

    if let Err(_) = shift_to(rdr, 8) {
        return Err(ReadErrors::CannotSeek);
    }

    let mut bytes_read: usize = BLOCK_43_HEADER_LENGTH;
    let mut wavelength: u32 = wl_low;
    let mut absorbance: f32 = 0.0;
    loop {
        if wavelength > wl_high {
            return Err(ReadErrors::InvalidFrame);
        }

        let raw = read_value::<i16, T>(rdr, Endianess::Little)?;
        bytes_read += mem::size_of::<i16>();

        if raw == SET_ABSORBANCE_ABSOLUTE {
            let a_abs = read_value::<i32, T>(rdr, Endianess::Little)?;
            bytes_read += mem::size_of::<i32>();

            absorbance = a_abs as f32 * scaling;
        } else {
            absorbance += raw as f32 * scaling;
        }

        spectrum.push(SpectPoint { wavelength: wavelength, absorbance: absorbance});

        if bytes_read == frame_length {
            break;
        }

        wavelength += wl_step;
    }
    
    if wavelength != wl_high {
        return Err(ReadErrors::InvalidFrame);
    }

    Ok((Spectrum { time: timestamp as f32 / 60000.0, spectrum: spectrum }, EndOfSpectra::No))
}

fn read_spectra<T: ReadSeek>(rdr: &mut T, interp: &Interpretation) -> Result<Vec<Spectrum>, ReadErrors> {
    let mut spectra: Vec<Spectrum> = Vec::new();

    if let Err(_) = seek_to::<T>(rdr, interp.data_offset) {
        return Err(ReadErrors::CannotSeek)
    }

    loop {
        let frame = read_frame::<T>(rdr, interp.scaling)?;
        match frame.1 {
            EndOfSpectra::Yes => if spectra.len() != interp.num_frames {
                return Err(ReadErrors::InvalidFileSize)
            } else {
                return Ok(spectra)
            },
            EndOfSpectra::No => spectra.push(frame.0),
        }
    }
}

pub fn read<T: ReadSeek>(rdr: &mut T) -> Result<Trace, ReadErrors> {
    let interpretation = read_interpretation_data::<T>(rdr)?;

    let metadata = read_metadata::<T>(rdr)?;
    let spectra = read_spectra::<T>(rdr, &interpretation)?;

    Ok(Trace{ metadata: metadata, spectra: spectra })
}
