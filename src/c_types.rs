
use std::os::raw::c_char;
use std::ffi::CString;
use std::mem;

use globals::*;

static CANNOT_OPEN_ERR_STR: &str = "Cannot open input file for reading\0";
static CANNOT_READ_ERR_STR: &str = "Cannot read input data\0";
static CANNOT_SEEK_ERR_STR: &str = "Cannot seek input data\0";
static INVALID_CALL_ERR_STR: &str = "Invalid public C interface call\0";
static INVALID_DATE_ERR_STR: &str = "Invalid date in input data\0";
static INVALID_FILE_SIZE_ERR_STR: &str = "Input data file has invalid size\0";
static INVALID_FRAME_ERR_ERR_STR: &str = "Input data contains invalid frame\0";
static INVALID_STRING_ERR_STR: &str = "Input data contains invalid string\0";
static UNSUPPORTED_ERR_STR: &str = "Unsupported file type\0";
static WRONG_FILE_TYPE_ERR_STR: &str = "Wrong input file type\0";


#[allow(non_camel_case_types)]
#[repr(C)]
pub enum LSPTZ_Result {
    LSPTZ_Success,
    LSPTZ_Failure
}

#[allow(non_camel_case_types)]
#[repr(C)]
pub enum LSPTZ_ReadErrors {
    LSPTZ_CannotOpen,
    LSPTZ_CannotRead,
    LSPTZ_CannotSeek,
    LSPTZ_InvalidCall,
    LSPTZ_InvalidDate,
    LSPTZ_InvalidFileSize,
    LSPTZ_InvalidFrame,
    LSPTZ_InvalidString,
    LSPTZ_Unsupported,
    LSPTZ_WrongFileType
}

#[repr(C)]
pub struct LSPTZ_Metadata {
    pub sample_name: *const c_char,
    pub operator_name: *const c_char,
    pub units: *const c_char,
    pub signal_origin: *const c_char,
    pub method_name: *const c_char,
    pub run_number: u16,
    pub day: u8,
    pub month: u8,
    pub year: u16,
    pub hour: u8,
    pub minute: u8,
    pub second: u8,
}

#[repr(C)]
pub struct LSPTZ_SpectPoint {
    pub wavelength: u32,
    pub absorbance: f32,
}

#[repr(C)]
pub struct LSPTZ_Spectrum {
    pub time: f32,
    pub spectrum: *const LSPTZ_SpectPoint,
    pub n_points: usize,
}

#[repr(C)]
pub struct LSPTZ_Trace {
    pub metadata: LSPTZ_Metadata,
    pub spectra: *const LSPTZ_Spectrum,
    pub n_spectra: usize,
}

pub fn free_c_result(c_trace: &mut LSPTZ_Trace) {
    unsafe {
        /* Free metadata */
        {
            CString::from_raw(c_trace.metadata.sample_name as *mut c_char);
            CString::from_raw(c_trace.metadata.operator_name as *mut c_char);
            CString::from_raw(c_trace.metadata.units as *mut c_char);
            CString::from_raw(c_trace.metadata.signal_origin as *mut c_char);
            CString::from_raw(c_trace.metadata.method_name as *mut c_char);
        }

        {
            let spectra: Vec<LSPTZ_Spectrum> = Vec::from_raw_parts(c_trace.spectra as *mut LSPTZ_Spectrum,
                                                                   c_trace.n_spectra, c_trace.n_spectra);
            for spm in spectra {
                Vec::from_raw_parts(spm.spectrum as *mut LSPTZ_SpectPoint,
                                    spm.n_points, spm.n_points);
            }
        }
    }
}

pub fn error_string_c(error: LSPTZ_ReadErrors) -> *const c_char {
    match error {
        LSPTZ_ReadErrors::LSPTZ_CannotOpen => CANNOT_OPEN_ERR_STR.as_ptr() as *const c_char,
        LSPTZ_ReadErrors::LSPTZ_CannotRead => CANNOT_READ_ERR_STR.as_ptr() as *const c_char,
        LSPTZ_ReadErrors::LSPTZ_CannotSeek => CANNOT_SEEK_ERR_STR.as_ptr() as *const c_char,
        LSPTZ_ReadErrors::LSPTZ_InvalidDate => INVALID_DATE_ERR_STR.as_ptr() as *const c_char,
        LSPTZ_ReadErrors::LSPTZ_InvalidFileSize => INVALID_FILE_SIZE_ERR_STR.as_ptr() as *const c_char,
        LSPTZ_ReadErrors::LSPTZ_InvalidFrame => INVALID_FRAME_ERR_ERR_STR.as_ptr() as *const c_char,
        LSPTZ_ReadErrors::LSPTZ_InvalidString => INVALID_STRING_ERR_STR.as_ptr() as *const c_char,
        LSPTZ_ReadErrors::LSPTZ_Unsupported => UNSUPPORTED_ERR_STR.as_ptr() as *const c_char,
        LSPTZ_ReadErrors::LSPTZ_WrongFileType => WRONG_FILE_TYPE_ERR_STR.as_ptr() as *const c_char,
        LSPTZ_ReadErrors::LSPTZ_InvalidCall => INVALID_CALL_ERR_STR.as_ptr() as *const c_char,
    }
}

pub fn metadata_to_c_metadata(metadata: &Metadata) -> LSPTZ_Metadata {
    let c_sample_name = CString::new(metadata.sample_name.clone()).unwrap();
    let c_operator_name = CString::new(metadata.operator_name.clone()).unwrap();
    let c_units = CString::new(metadata.units.clone()).unwrap();
    let c_signal_origin = CString::new(metadata.signal_origin.clone()).unwrap();
    let c_method_name = CString::new(metadata.method_name.clone()).unwrap();

    let m = LSPTZ_Metadata {
        sample_name: c_sample_name.as_ptr(),
        operator_name: c_operator_name.as_ptr(),
        units: c_units.as_ptr(),
        signal_origin: c_signal_origin.as_ptr(),
        method_name: c_method_name.as_ptr(),
        run_number: metadata.run_number,
        day: metadata.day,
        month: metadata.month,
        year: metadata.year,
        hour: metadata.hour,
        minute: metadata.minute,
        second: metadata.second,
    };

    mem::forget(c_sample_name);
    mem::forget(c_operator_name);
    mem::forget(c_units);
    mem::forget(c_signal_origin);
    mem::forget(c_method_name);

    m
}

pub fn readerror_to_c_readerror(error: ReadErrors) -> LSPTZ_ReadErrors {
    match error {
        ReadErrors::CannotOpen => LSPTZ_ReadErrors::LSPTZ_CannotOpen,
        ReadErrors::CannotRead => LSPTZ_ReadErrors::LSPTZ_CannotRead,
        ReadErrors::CannotSeek => LSPTZ_ReadErrors::LSPTZ_CannotSeek,
        ReadErrors::InvalidDate => LSPTZ_ReadErrors::LSPTZ_InvalidDate,
        ReadErrors::InvalidFileSize => LSPTZ_ReadErrors::LSPTZ_InvalidFileSize,
        ReadErrors::InvalidFrame => LSPTZ_ReadErrors::LSPTZ_InvalidFrame,
        ReadErrors::InvalidString => LSPTZ_ReadErrors::LSPTZ_InvalidString,
        ReadErrors::Unsupported => LSPTZ_ReadErrors::LSPTZ_Unsupported,
        ReadErrors::WrongFileType => LSPTZ_ReadErrors::LSPTZ_WrongFileType,
    }
}

pub fn spectra_to_c_spectra(spectra: &Vec<Spectrum>) -> *const LSPTZ_Spectrum {
    let mut v: Vec<LSPTZ_Spectrum> = Vec::new();

    for spm in spectra {
        let c_spt = LSPTZ_Spectrum{ time: spm.time,
                                    spectrum: spectrum_to_c_spectrum(&spm.spectrum),
                                    n_points: spm.spectrum.len(), };

        v.push(c_spt);
    }


    v.shrink_to_fit();

    let ptr = v.as_ptr();
    mem::forget(v);

    ptr
}


fn spectrum_to_c_spectrum(spectrum: &Vec<SpectPoint>) -> *const LSPTZ_SpectPoint {
    let mut v: Vec<LSPTZ_SpectPoint> = Vec::new();

    for pt in spectrum {
        v.push(LSPTZ_SpectPoint{ wavelength: pt.wavelength, absorbance: pt.absorbance });
    }

    v.shrink_to_fit();

    let ptr = v.as_ptr();
    mem::forget(v);

    ptr
}
