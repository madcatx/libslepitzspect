use std::io::SeekFrom;
use std::mem;
use globals::*;

pub trait ReadSeek: std::io::Read + std::io::Seek {}
impl<T: std::io::Read + std::io::Seek> ReadSeek for T {}

pub enum Endianess {
    Little,
    Big,
}

pub trait BlobReadable<T> {
    const SIZE: usize;

    fn from_bytes(bytes: &[u8]) -> T;
    fn from_be(v: T) -> T;
    fn from_le(v: T) -> T;
}

impl BlobReadable<i8> for i8 {
    const SIZE: usize = mem::size_of::<i8>(); 

    fn from_bytes(bytes: &[u8]) -> i8 {
        bytes[0] as i8
    }

    fn from_be(v: i8) -> i8 {
        v
    }

    fn from_le(v: i8) -> i8 {
        v
    }
}

impl BlobReadable<u8> for u8 {
    const SIZE: usize = mem::size_of::<u8>();

    fn from_bytes(bytes: &[u8]) -> u8 {
        bytes[0] as u8
    }

    fn from_be(v: u8) -> u8 {
        v
    }

    fn from_le(v: u8) -> u8 {
        v
    }
}

impl BlobReadable<i16> for i16 {
    const SIZE: usize = mem::size_of::<i16>();

    fn from_bytes(bytes: &[u8]) -> i16 {
        (bytes[0] as i16) + ((bytes[1] as i16) << 8)
    }

    fn from_be(v: i16) -> i16 {
        i16::from_be(v)
    }

    fn from_le(v: i16) -> i16 {
        i16::from_le(v)
    }
}

impl BlobReadable<u16> for u16 {
    const SIZE: usize = mem::size_of::<u16>();

    fn from_bytes(bytes: &[u8]) -> u16 {
        (bytes[0] as u16) + ((bytes[1] as u16) << 8)
    }

    fn from_be(v: u16) -> u16 {
        u16::from_be(v)
    }

    fn from_le(v: u16) -> u16 {
        u16::from_le(v)
    }
}

impl BlobReadable<i32> for i32 {
    const SIZE: usize = mem::size_of::<i32>();

    fn from_bytes(bytes: &[u8]) -> i32 {
        (bytes[0] as i32) + ((bytes[1] as i32) << 8) + ((bytes[2] as i32) << 16) + ((bytes[3] as i32) << 24)
    }

    fn from_be(v: i32) -> i32 {
        i32::from_be(v)
    }

    fn from_le(v: i32) -> i32 {
        i32::from_le(v)
    }
}

impl BlobReadable<f32> for f32 {
    const SIZE: usize = mem::size_of::<f32>();

    fn from_bytes(bytes: &[u8]) -> f32 {
        let r = (bytes[0] as u32) + ((bytes[1] as u32) << 8) + ((bytes[2] as u32) << 16) + ((bytes[3] as u32) << 24);

        f32::from_bits(r)
    }

    fn from_be(v: f32) -> f32 {
        let bits = unsafe { mem::transmute::<f32, u32>(v) };
        let flipped = u32::from_be(bits);

        let r = unsafe { mem::transmute::<u32, f32>(flipped) };
        r
    }

    fn from_le(v: f32) -> f32 {
        let bits = unsafe { mem::transmute::<f32, u32>(v) };
        let flipped = u32::from_le(bits);

        let r = unsafe { mem::transmute::<u32, f32>(flipped) };
        r
    }
}

impl BlobReadable<f64> for f64 {
    const SIZE: usize = mem::size_of::<f64>();

    fn from_bytes(bytes: &[u8]) -> f64 {
        let r = (bytes[0] as u64) + ((bytes[1] as u64) << 8) + ((bytes[2] as u64) << 16) + ((bytes[3] as u64) << 24) + 
                ((bytes[4] as u64) << 32) + ((bytes[5] as u64) << 40) + ((bytes[6] as u64) << 48) + ((bytes[7] as u64) << 56);

        f64::from_bits(r)
    }

    fn from_be(v: f64) -> f64 {
        let bits = unsafe { mem::transmute::<f64, u64>(v) };
        let flipped = u64::from_be(bits);

        let r = unsafe { mem::transmute::<u64, f64>(flipped) };
        r
    }

    fn from_le(v: f64) -> f64 {
        let bits = unsafe { mem::transmute::<f64, u64>(v) };
        let flipped = u64::from_le(bits);

        let r = unsafe { mem::transmute::<u64, f64>(flipped) };
        r
    }
}

impl BlobReadable<u32> for u32 {
    const SIZE: usize = mem::size_of::<u32>();

    fn from_bytes(bytes: &[u8]) -> u32 {
        (bytes[0] as u32) + ((bytes[1] as u32) << 8) + ((bytes[2] as u32) << 16) + ((bytes[3] as u32) << 24)
    }

    fn from_be(v: u32) -> u32 {
        u32::from_be(v)
    }

    fn from_le(v: u32) -> u32 {
        u32::from_le(v)
    }
}

pub fn shift_to<T: std::io::Seek>(rdr: &mut T, shift: i64) -> ::std::io::Result<u64> {
    rdr.seek(SeekFrom::Current(shift))
}

pub fn seek_to<T: std::io::Seek>(rdr: &mut T, offset: u64) -> ::std::io::Result<u64> {
    rdr.seek(SeekFrom::Start(offset))
}

pub fn read_blob<T: BlobReadable<T>, U: ReadSeek>(rdr: &mut U) -> Result<T, ReadErrors> {
    let mut v: Vec<u8> = Vec::new();
    v.resize(T::SIZE, 0);

    if let Err(_) = rdr.read_exact(&mut v) {
        return Err(ReadErrors::CannotRead);
    }

    Ok(T::from_bytes(&v))
}

pub fn read_blob_at<T: BlobReadable<T>, U: ReadSeek>(rdr: &mut U, offset: u64) -> Result<T, ReadErrors> {
    if let Err(_) = seek_to::<U>(rdr, offset) {
        return Err(ReadErrors::CannotSeek);
    }

    read_blob::<T, U>(rdr)
}

pub fn read_value<T: BlobReadable<T>, U: ReadSeek>(rdr: &mut U, endianess: Endianess) -> Result<T, ReadErrors> {
    let r = read_blob::<T, U>(rdr)?;

    match endianess {
        Endianess::Big => return Ok(T::from_be(r)),
        Endianess::Little => return Ok(T::from_le(r)),
    }
}

pub fn read_value_at<T: BlobReadable<T>, U: ReadSeek>(rdr: &mut U, offset: u64, endianess: Endianess) -> Result<T, ReadErrors> {
    let r = read_blob_at::<T, U>(rdr, offset)?;

    match endianess {
        Endianess::Big => return Ok(T::from_be(r)),
        Endianess::Little => return Ok(T::from_le(r)),
    }
}

pub fn read_ascii_string_at<T: ReadSeek>(rdr: &mut T, offset: u64) -> Result<String, ReadErrors> {
    let len = read_blob_at::<u8, T>(rdr, offset)?;

    if len == 0 {
        return Ok(String::from(""));
    }

    let mut s: String = String::from("");

    for _ in 0 .. len {
        let byte = read_blob::<u8, T>(rdr)?;
        s.push(byte as char);
    }

    Ok(s)
}

pub fn read_utf16_string_at<T: ReadSeek>(rdr: &mut T, offset: u64) -> Result<String, ReadErrors> {
    let len = read_blob_at::<u8, T>(rdr, offset)?;

    if len == 0 {
        return Ok(String::from(""));
    }

    let mut v: Vec<u16> = Vec::new();

    for _ in 0 .. len {
        let cpt = read_value::<u16, T>(rdr, Endianess::Little)?;
        v.push(cpt);
    }

    match String::from_utf16(&v) {
        Err(_) => return Err(ReadErrors::InvalidString),
        Ok(s) => return Ok(s),
    };
}
