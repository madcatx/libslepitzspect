mod c_types;
mod globals;
mod file_ops;
mod one_three_one;

use std::ffi::CStr;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use globals::*;
pub use globals::Trace;
pub use globals::ReadErrors;
use file_ops::*;
use c_types::*;

const OFFSET_FILE_ID_NUM_131: u64 = 0x00FB;
const OFFSET_FILE_ID_STR_TWO_131: u64 = 0x015B;

const _UV_FILE_ID_NUM_31: u8 = 31;
static UV_FILE_ID_STR_31: &str = "31";
const UV_FILE_ID_NUM_131: u8 = 131;
static UV_FILE_ID_STR_131: &str = "131";
static UV_FILE_ID_STR_TWO_131: &str = "LC DATA FILE";

static CANNOT_OPEN_ERR_STR: &str = "Cannot open input file for reading";
static CANNOT_READ_ERR_STR: &str = "Cannot read input data";
static CANNOT_SEEK_ERR_STR: &str = "Cannot seek input data";
static INVALID_DATE_ERR_STR: &str = "Invalid date in input data";
static INVALID_FILE_SIZE_ERR_STR: &str = "Input data file has invalid size";
static INVALID_FRAME_ERR_ERR_STR: &str = "Input data contains invalid frame";
static INVALID_STRING_ERR_STR: &str = "Input data contains invalid string";
static UNSUPPORTED_ERR_STR: &str = "Unsupported file type";
static WRONG_FILE_TYPE_ERR_STR: &str = "Wrong input file type";

fn check_signature<T: ReadSeek>(rdr: &mut T) -> Result<UVFileType, ReadErrors> {
    let id_str = read_ascii_string_at(rdr, 0x0)?;

    let uv_type = if id_str == UV_FILE_ID_STR_31 {
        UVFileType::Type31
    } else if id_str == UV_FILE_ID_STR_131 {
        UVFileType::Type131
    } else {
        return Err(ReadErrors::WrongFileType);
    };

    match uv_type {
        UVFileType::Type31 => return Err(ReadErrors::Unsupported),
        UVFileType::Type131 => {
            let num_chk = read_blob_at::<u8, T>(rdr, OFFSET_FILE_ID_NUM_131)?;
            if num_chk != UV_FILE_ID_NUM_131 {
                return Err(ReadErrors::WrongFileType);
            }

            let id_str_two = read_utf16_string_at::<T>(rdr, OFFSET_FILE_ID_STR_TWO_131)?;
            if id_str_two != UV_FILE_ID_STR_TWO_131 {
                return Err(ReadErrors::WrongFileType)
            }
        }
    };

    Ok(uv_type)
}

fn read_spectrum_file(path: &String) -> Result<Trace, ReadErrors> {
    let io_path = Path::new(path);

    let fh = match File::open(&io_path) {
        Err(_) => return Err(ReadErrors::CannotOpen),
        Ok(r) => r,
    };

    let mut rdr = BufReader::with_capacity(131072, fh);

    let uv_type = check_signature(&mut rdr)?;

    match uv_type {
        UVFileType::Type31 => return Err(ReadErrors::Unsupported),
        UVFileType::Type131 => return one_three_one::read(&mut rdr),
    }
}

pub fn error_string(error: ReadErrors) -> &'static str {
    match error {
        ReadErrors::CannotOpen => CANNOT_OPEN_ERR_STR,
        ReadErrors::CannotRead => CANNOT_READ_ERR_STR,
        ReadErrors::CannotSeek => CANNOT_SEEK_ERR_STR,
        ReadErrors::InvalidDate => INVALID_DATE_ERR_STR,
        ReadErrors::InvalidFileSize => INVALID_FILE_SIZE_ERR_STR,
        ReadErrors::InvalidFrame => INVALID_FRAME_ERR_ERR_STR,
        ReadErrors::InvalidString => INVALID_STRING_ERR_STR,
        ReadErrors::Unsupported => UNSUPPORTED_ERR_STR,
        ReadErrors::WrongFileType => WRONG_FILE_TYPE_ERR_STR,
    }
}

pub fn read(path: &String) -> Result<Trace, ReadErrors> {
    read_spectrum_file(&path)
}

#[no_mangle]
pub extern "system" fn lsptz_error_string(error: LSPTZ_ReadErrors) -> *const std::os::raw::c_char {
    error_string_c(error)
}

#[no_mangle]
pub extern "system" fn lsptz_read(path: *const std::os::raw::c_char, trace: &mut LSPTZ_Trace, error: &mut LSPTZ_ReadErrors) -> LSPTZ_Result {
    if path.is_null() {
        *error = LSPTZ_ReadErrors::LSPTZ_InvalidCall;
        return LSPTZ_Result::LSPTZ_Failure;
    }

    let path_wrap = unsafe { CStr::from_ptr(path) };
    let nat_path = match path_wrap.to_str() {
        Err(_) =>  {
            *error = LSPTZ_ReadErrors::LSPTZ_InvalidCall;
            return LSPTZ_Result::LSPTZ_Failure;
        },
        Ok(s) => s,
    };


    let nat_trace = match read(&String::from(nat_path)) {
        Err(e) => {
            *error = readerror_to_c_readerror(e);
            return LSPTZ_Result::LSPTZ_Failure;
        },
        Ok(s) => s,
    };

    trace.metadata = metadata_to_c_metadata(&nat_trace.metadata);
    trace.spectra = spectra_to_c_spectra(&nat_trace.spectra);

    trace.n_spectra = nat_trace.spectra.len();

    LSPTZ_Result::LSPTZ_Success
}

#[no_mangle]
pub extern "system" fn lsptz_free(trace: &mut LSPTZ_Trace) {
    free_c_result(trace);
}
