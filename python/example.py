#! /usr/bin/env python3

from slepitzspect import Slepitzspect
import sys

LIBRARY_PATH = '/home/madcat/Devel/ECHMET/Spectractor/slepitzspect/target/debug/libslepitzspect.so'

lsptz = Slepitzspect(LIBRARY_PATH)

res = lsptz.read(sys.argv[1])

if res[0] is True:
    print(res[1].metadata.sample_name)
    print(res[1].metadata.method_name)

    for time, spectrum in res[1].spectra.items():
        print('time: {}'.format(time))

        for wl, ab in spectrum.items():
            print('{}: {}; '.format(wl, ab))
else:
    print('Error: {} => {}'.format(res[1], res[2]))
