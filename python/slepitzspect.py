from ctypes import (CDLL, Structure, byref, c_char_p, c_uint8, c_uint16,
                    c_float, c_uint32, c_int32, c_void_p, POINTER)
import enum


LSPTZ_RESULT_SUCCESS = 0
LSPTZ_RESULT_FAILURE = 1

LSPTZ_ERR_CANNOT_OPEN = 0
LSPTZ_ERR_CANNOT_READ = 1
LSPTZ_ERR_CANNOT_SEEK = 2
LSPTZ_ERR_INVALID_CALL = 3
LSPTZ_ERR_INVALID_DATE = 4
LSPTZ_ERR_INVALID_FILE_SIZE = 5
LSPTZ_ERR_INVALID_FRAME = 6
LSPTZ_ERR_INVALID_STRING = 7
LSPTZ_ERR_UNSUPPORTED = 8
LSPTZ_ERR_WRONG_FILE_TYPE = 9


class LSPTZ_METADATA(Structure):
    _fields_ = [('sample_name', c_char_p),
                ('operator_name', c_char_p),
                ('units', c_char_p),
                ('signal_origin', c_char_p),
                ('method_name', c_char_p),
                ('run_number', c_uint16),
                ('day', c_uint8),
                ('month', c_uint8),
                ('year', c_uint16),
                ('hour', c_uint8),
                ('minute', c_uint8),
                ('second', c_uint8)]


class LSPTZ_SPECTPOINT(Structure):
    _fields_ = [('wavelength', c_uint32),
                ('absorbance', c_float)]


class LSPTZ_SPECTRUM(Structure):
    _fields_ = [('time', c_float),
                ('spectrum', POINTER(LSPTZ_SPECTPOINT)),
                ('n_points', c_void_p)]


class LSPTZ_TRACE(Structure):
    _fields_ = [('metadata', LSPTZ_METADATA),
                ('spectra', POINTER(LSPTZ_SPECTRUM)),
                ('n_spectra', c_void_p)]


class Slepitzspect:
    """Wrapper around `libSlepitzspect` library.
    """

    class InvalidReadErrorException(Exception):
        def __init__(self):
            super().__init__()
            self.msg = "Unrecognized LSPTZ_ReadError value"

    class ReadErrors(enum.Enum):
        """ Error return codes.
        """
        CannotOpen = 0
        CannotRead = 1
        CannotSeek = 2
        InvalidCall = 3
        InvalidDate = 4
        InvalidFileSize = 5
        InvalidFrame = 6
        InvalidString = 7
        Unsupported = 8
        WrongFileType = 9

    class Metadata:
        def __init__(self):
            self.sample_name = ""
            self.operator_name = ""
            self.units = ""
            self.signal_origin = ""
            self.method_name = ""
            self.run_number = 0
            self.day = 0
            self.month = 0
            self.year = 0
            self.hour = 0
            self.minute = 0
            self.second = 0

    class Trace:
        """UV trace.

        Attributes:
            metadata (:obj:Metadata) Signal metadata.
            spectra (dict) Two level dictionary.
                           First level has detection time as key and
                           a dictionary as value.
                           Second level has wavelength in picometres and
                           absorbance as unit.
                           Unit of absorbance can be retrieved
                           from metadata.
        """

        def __init__(self, metadata, spectra):
            self.metadata = metadata
            self.spectra = spectra

    @staticmethod
    def _mk_readerror(error):
        if error == LSPTZ_ERR_CANNOT_OPEN:
            return Slepitzspect.ReadErrors.CannotOpen
        if error == LSPTZ_ERR_CANNOT_READ:
            return Slepitzspect.ReadErrors.CannotRead
        if error == LSPTZ_ERR_CANNOT_SEEK:
            return Slepitzspect.ReadErrors.CannotSeek
        if error == LSPTZ_ERR_INVALID_CALL:
            return Slepitzspect.ReadErrors.InvalidCall
        if error == LSPTZ_ERR_INVALID_DATE:
            return Slepitzspect.ReadErrors.InvalidDate
        if error == LSPTZ_ERR_INVALID_FILE_SIZE:
            return Slepitzspect.ReadErrors.InvalidFileSize
        if error == LSPTZ_ERR_INVALID_FRAME:
            return Slepitzspect.ReadErrors.InvalidFrame
        if error == LSPTZ_ERR_INVALID_STRING:
            return Slepitzspect.ReadErrors.InvalidString
        if error == LSPTZ_ERR_UNSUPPORTED:
            return Slepitzspect.ReadErrors.Unsupported
        if error == LSPTZ_ERR_WRONG_FILE_TYPE:
            return Slepitzspect.ReadErrors.WrongFileType

        raise Slepitzspect.InvalidReadErrorException()

    @staticmethod
    def _mk_trace(c_trace):
        metadata = Slepitzspect.Metadata()
        metadata.sample_name = c_trace.metadata.sample_name.decode('UTF-8')
        metadata.operator_name = c_trace.metadata.operator_name.decode('UTF-8')
        metadata.units = c_trace.metadata.units.decode('UTF-8')
        metadata.signal_origin = c_trace.metadata.signal_origin.decode('UTF-8')
        metadata.method_name = c_trace.metadata.method_name.decode('UTF-8')
        metadata.run_number = c_trace.metadata.run_number
        metadata.day = c_trace.metadata.day
        metadata.month = c_trace.metadata.month
        metadata.year = c_trace.metadata.year
        metadata.hour = c_trace.metadata.hour
        metadata.minute = c_trace.metadata.minute
        metadata.second = c_trace.metadata.second

        spectra = dict()

        for idx in range(0, c_trace.n_spectra):
            raw_spectrum = c_trace.spectra[idx]

            spm = dict()
            for jdx in range(0, raw_spectrum.n_points):
                wl = raw_spectrum.spectrum[jdx].wavelength
                ab = raw_spectrum.spectrum[jdx].absorbance

                spm[wl] = ab

            spectra[raw_spectrum.time] = spm

        return Slepitzspect.Trace(metadata, spectra)

    def __init__(self, path):
        """:obj:Slepitzspect constructor.

        Args:
            path (str): Path to the `libSlepitzspect` library.

        Raises:
            AttributeError: Required symbol was not found in the library.
        """

        self.lib_obj = CDLL(path)

        def check_symbol(sym):
            if not hasattr(self.lib_obj, sym):
                raise AttributeError('Undefined symbol: {}'.format(sym))

        check_symbol('lsptz_error_string')
        check_symbol('lsptz_free')
        check_symbol('lsptz_read')

        self.lib_obj.lsptz_error_string.restype = c_char_p

    def read(self, path):
        """Reads a UV file from a given path

        Args:
            path (str): Path to the file to be read

        Returns:
            (bool, Trace) tuple when the read succeeds. First element is set
                          to True.
            (bool, ReadErrors, ErrorString) tuple then the read fails.
                                            First element is set to False,
                                            second element contains the error
                                            code, third element contains
                                            error description string.
        """

        error = c_int32()
        c_trace = LSPTZ_TRACE()

        ret = self.lib_obj.lsptz_read(path.encode('UTF-8'), byref(c_trace),
                                      byref(error))
        if ret == LSPTZ_RESULT_FAILURE:
            err_str = self.lib_obj.lsptz_error_string(error).decode('UTF-8')
            return (False, self._mk_readerror(error.value), err_str)

        trace = self._mk_trace(c_trace)
        self.lib_obj.lsptz_free(byref(c_trace))

        return (True, trace)
